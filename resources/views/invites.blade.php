<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invites</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
    <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=en-de"
            type="text/javascript"></script>

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .preview {
            padding-top: 20px;
        }

        #message {
            padding-top: 20px;
        }
    </style>
</head>
<body>
    @if(session()->has('message'))
        <div id="message" class="container">
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        </div>
    @endif

    <div class="container">
        <label class="preview">Please, fill out the form:</label>
        <form method="post">
            {!! csrf_field() !!}
            <div id="app">
                <auto-fill :users="{{$users}}"></auto-fill>
            </div>
        </form>
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
    <script type="text/javascript">
      window.onload = function () {
        jQuery("#user-city").val(ymaps.geolocation.city);
        jQuery("#country").val(ymaps.geolocation.country);
      }
    </script>
</body>
</html>
