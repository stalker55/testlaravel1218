<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InviteControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_can_view_index_page()
    {
        $this->get('/')
            ->assertStatus(200)
            ->assertViewIs('invites');
    }

    /** @test */
    public function a_can_store_invoices()
    {
        $data = [
            'name' => [
                'Andy Low',
                'John Doe'
            ],
            'phone' => [
                '7894561111',
                '4567892222'
            ],
            'user-city' => 'Moscow',
            'country'   => 'Russia',
        ];

        $this->post('/', $data)
            ->assertSessionHas('message', 'Success! 2 entries have been added. Leader: Andy Low');
        $this->assertDatabaseHas('invites', [
            'name'  => $data['name'][0],
            'phone' => $data['phone'][0],
        ]);
        $this->assertDatabaseHas('invites', [
            'name'  => $data['name'][1],
            'phone' => $data['phone'][1],
        ]);
    }
}
