<?php

namespace App\Http\Controllers;

use App\Classes\Shopify;
use App\Classes\Weather;
use App\Http\Requests\InviteStoreRequest;
use App\Models\Invite;

class InviteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Shopify $shopify
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Shopify $shopify)
    {
        return view('invites', ['users' => $shopify->getCustomers()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InviteStoreRequest $request
     * @param Weather $weather
     *
     * @return \Illuminate\Http\Response
     */
    public function store(InviteStoreRequest $request, Weather $weather)
    {
        $invites  = $request->validated();
        $weather  = $weather->getWeather($invites['user-city'], $invites['country']);

        foreach ($invites['name'] as $key => $name) {
            Invite::create([
                'name'     => $name,
                'phone'    => $invites['phone'][$key],
                'country'  => $invites['user-city'],
                'weather'  => $weather,
                'isLeader' => (bool) !$key
            ]);
        }

        return back()->with('message', 'Success! ' . count($invites['name']) . ' entries have been added. Leader: ' . $invites['name'][0]);
    }
}
