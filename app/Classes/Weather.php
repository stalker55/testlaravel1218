<?php

namespace App\Classes;

class Weather
{
    public function getWeather($city, $country)
    {
        $params = http_build_query([
            'q'     => "{$city},{$country}",
            'cnt'   => 1,
            'lang'  => 'en',
            'units' => 'metric',
            'appid' => env('WEATHER_ID'),
        ]);
        $url = 'http://api.openweathermap.org/data/2.5/forecast?' . $params;
        $data = @file_get_contents($url);

        return json_encode(json_decode($data)->list[0]->main);
    }
}