<?php

namespace App\Classes;

use PHPShopify\ShopifySDK;

class Shopify
{
    private $shopify;

    public function __construct()
    {
        $this->shopify = new ShopifySDK([
            'ShopUrl'  => env('SHOPIFY_REDIRECT'),
            'ApiKey'   => env('SHOPIFY_KEY'),
            'Password' => env('SHOPIFY_SECRET'),
        ]);
    }

    public function getCustomers()
    {
        $users = $this->shopify->Customer->get();
        array_shift($users);

        $collection = collect();
        foreach ($users as $user) {
            $collection->push((object) [
                'name'  => $user['first_name'] . ' ' . $user['last_name'],
                'phone' => $user['default_address']['phone']
            ]);
        }

        return $collection;
    }
}